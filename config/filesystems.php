<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
        ],

        'folder' => [
            'driver' => 'local',
            'root'   => storage_path('app/folder'),
            'url' => env('APP_URL').'/storage/app/folder',
        ],

        'emulator' => [
            'driver' => 'local',
            'root'   => storage_path('app/emulator'),
            'url' => env('APP_URL').'/storage/app/emulator',
        ],

        'disk1' => [
            'driver' => 'local',
            'root'   => storage_path('app/disk1'),
            'url' => env('APP_URL').'/storage/app/disk1',
        ],


        env('CASTING_DRIVER') === 'local' ? 'kino' : 'kino_not_used' => [
            'driver' => 'local',
            'root'   => storage_path('app/kino'),
            'url' => env('APP_URL').'/storage/app/kino',
        ],

        env('CASTING_DRIVER') === 'smb' ? 'kino' : 'kino_not_used' => [
            'driver'    => 'smb',
            'host'      => env('SAMBA_HOST'),
            'username'  => env('SAMBA_USER'),
            'password'  => env('SAMBA_PASSWORD'),
            'workgroup' => 'WORKGROUP', // OR DOMAIN
            'path'      => env('SAMBA_PATH'),
        ],

        'temp_videos' => [
            'driver' => 'local',
            'root'   => storage_path('tmp'),
            'url' => env('APP_URL').'/storage/tmp',
        ],

        'movie_screenshots' => [
            'driver' => 'local',
            'root'   => storage_path('movies'),
            'url' => env('APP_URL').'/storage/movies',
            'visibility' => 'public'
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */

    'links' => [
        public_path('storage') => storage_path('app/public'),
    ],

];
