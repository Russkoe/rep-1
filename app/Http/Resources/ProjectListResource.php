<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Kino\Helper;

class ProjectListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $rolesCount = $this->roles()->count();
        $actorsCount =  $this->auditions()->distinct('actor_id')->count();
        $auditionsCount =  $this->auditions()->count();

        $data = [
            'id'              => $this->id,
            'name'            => $this->name,
            'year_end'        => $this->year_end,
            'roles_count'     => $rolesCount > 0 ?
                ($rolesCount.' '. Helper::plural(['роль', 'роли', 'ролей'], $rolesCount).', ') : '',
            'actors_count'    => $actorsCount > 0 ?
                ($actorsCount.' '. Helper::plural(['актер', 'актера', 'актеров'], $actorsCount).', ') : '',
            'auditions_count' => $auditionsCount > 0 ?
                ($auditionsCount.' '. Helper::plural(['проба', 'пробы', 'проб'], $auditionsCount)) : '',
        ];

        return $data;
    }
}
