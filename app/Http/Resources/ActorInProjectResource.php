<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ActorInProjectResource extends JsonResource
{
    protected $addedAuditions = [];

    public function addAudition(AuditionResource $audition)
    {
        $this->addedAuditions[] = $audition;
    }


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'       => $this->id,
            'name'     => $this->name,
            'auditions' => $this->addedAuditions,
        ];
    }
}
