<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RoleCollection extends ResourceCollection
{

    /**
     * @var integer
     */
    protected $actorId;

    /**
     * @param $actorId
     * @return $this
     */
    public function setActorId($actorId)
    {
        $this->actorId = $actorId;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->actorId) {
            return $this->collection->map(function (RoleResource $resource) use ($request) {
                return $resource->setActorId($this->actorId)->toArray($request);
            })->all();
        }

        return parent::toArray($request);
    }
}
