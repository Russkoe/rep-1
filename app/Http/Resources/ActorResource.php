<?php

namespace App\Http\Resources;

use App\Models\Audition;
use Illuminate\Http\Resources\Json\JsonResource;

class ActorResource extends JsonResource
{
    protected $addedAuditions = [];
    protected $withAuditions = false;
    protected $withMoviesGroupedByProject = false;

    public function addAudition(AuditionResource $audition)
    {
        $this->addedAuditions[] = $audition;
    }

    public function withAuditions(bool $with = true): self
    {
        $this->withAuditions = $with;
        return $this;
    }

    public function withMoviesGroupedByProject(bool $with = true): self
    {
        $this->withMoviesGroupedByProject = $with;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'        => $this->id,
            'name'      => $this->name,
            'roles'     => $this->roles,
            'auditions' => $this->withAuditions ? AuditionResource::collection($this->auditions) :
                (count($this->addedAuditions) > 0 ? $this->addedAuditions : $this->auditions),
        ];


        if($this->withMoviesGroupedByProject) {
            $grouped = $this->auditions()
                ->with('project')
                ->join('projects', 'auditions.project_id', '=', 'projects.id')
                ->orderBy('projects.sort_ord', 'ASC')
                ->groupBy('project_id')
                ->get();

            $info =  [];
            foreach ($grouped as $item) {
                $project = $item->project;

                $auditions = Audition::where('project_id', $project->id)->where('actor_id', $this->id)->get();
                $movies = [];
                foreach ($auditions as $audition) {
                    $movies[] = new MovieResource($audition->movie);
                }

                $info[] = [
                    'project'  => $project,
                    'movies'   => $movies,
                ];
            }

            $data['moviesGroupedByProject'] = $info;
        }

        return $data;
    }
}
