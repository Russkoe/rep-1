<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MovieResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $explode = explode('/', $this->file_path);

        return [
            'id'         => $this->id,
            'project_id' => $this->project_id,
            'name'       => $this->name,
            'file_path'  => $this->file_path,
            'file_name'  => end($explode),
            'dir_name'   => pathinfo($this->file_path, PATHINFO_DIRNAME),
            'image_path' => route('screenshotImage.displayImage', $this->id),
            'extension'  => $this->extension,
            'size'       => $this->size,
            'ext'        => $this->extension,
            'source'     => route('movie', $this->id),
        ];
    }

}