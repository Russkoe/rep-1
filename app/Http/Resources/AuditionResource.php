<?php

namespace App\Http\Resources;

use App\Kino\PathParser;
use Illuminate\Http\Resources\Json\JsonResource;

class AuditionResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $roles = $this->movie ? (new PathParser($this->movie->file_path))->parseRoles() : [];
        $otherRoles = [];
        foreach ($roles as $actorName => $roleName) {
            if ($actorName !== $this->actor->name) {
                $otherRoles[$actorName] = $roleName;
            }
        }

        return [
            'role'        => $this->role,
            'actor'       => $this->actor,
            'movie'       => new MovieResource($this->movie),
            'otherActors' => $otherRoles,
            'name'        => $this->movie ? $this->movie->name : '',
            'project'     => $this->project,
        ];
    }

}