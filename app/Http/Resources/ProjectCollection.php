<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProjectCollection extends ResourceCollection
{
    /**
     * @var integer
     */
    protected $actorId;

    protected $withRoles = true;
    protected $withActors = true;
    protected $withMovies = true;

    /**
     * @param $actorId
     * @return $this
     */
    public function setActorId($actorId)
    {
        $this->actorId = $actorId;
        return $this;
    }

    public function withActors(bool $with = true): self
    {
        $this->withActors = $with;
        return $this;
    }

    public function withRoles(bool $with = true): self
    {
        $this->withRoles = $with;
        return $this;
    }

    public function withMovies(bool $with = true): self
    {
        $this->withMovies = $with;
        return $this;
    }


    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function (ProjectResource $resource) use ($request) {
            return $resource
                ->setActorId($this->actorId)
                ->withRoles($this->withRoles)
                ->withMovies($this->withMovies)
                ->withActors($this->withActors)
                ->toArray($request);
        })->all();



        return parent::toArray($request);
    }
}
