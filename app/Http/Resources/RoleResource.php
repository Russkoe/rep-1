<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RoleResource extends JsonResource
{
    /**
     * @var integer
     */
    protected $actorId;

    /**
     * @param $actorId
     * @return $this
     */
    public function setActorId($actorId)
    {
        $this->actorId = $actorId;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'actors'     => new ActorCollection($this->actors),
            'project_id' => $this->project_id
        ];
    }

    public static function collection($resource){
        return new RoleCollection($resource, get_called_class());
    }
}
