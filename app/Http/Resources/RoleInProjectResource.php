<?php

namespace App\Http\Resources;

use App\Kino\Helper;
use App\Models\Audition;
use App\Services\Cache\StaticCache;
use Illuminate\Http\Resources\Json\JsonResource;

class RoleInProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $auditionsInPoject = StaticCache::get('auditions_in_project_' . $this->project_id);
        $auditions = $auditionsInPoject[$this->id] ?? [];

        if(empty($auditionsInPoject)) {
           $auditions =  Audition::where('project_id', $this->project_id)
                           ->where('project_role_id', $this->id)
                           ->with(['actor' => function ($query) {
                               $query->orderBy('name', 'desc');
                           }])->get();
        }

        return [
            'id'         => $this->id,
            'name'       => Helper::ucfirst($this->name),
            'auditions'  => new AuditionCollection($auditions),
            'project_id' => $this->project_id
        ];
    }

    public static function collection($resource){
        return new RoleCollection($resource, get_called_class());
    }
}
