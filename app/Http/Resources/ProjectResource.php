<?php

namespace App\Http\Resources;

use App\Models\Actor;
use App\Models\Audition;
use App\Services\Cache\StaticCache;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * @var integer
     */
    protected $actorId;

    protected $withRoles = true;
    protected $withActors = true;
    protected $withMovies = true;

    /**
     * @param $actorId
     * @return $this
     */
    public function setActorId($actorId)
    {
        $this->actorId = $actorId;
        return $this;
    }

    public function withActors(bool $with = true): self
    {
        $this->withActors = $with;
        return $this;
    }

    public function withRoles(bool $with = true): self
    {
        $this->withRoles = $with;
        return $this;
    }

    public function withMovies(bool $with = true): self
    {
        $this->withMovies = $with;
        return $this;
    }


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'        => $this->id,
            'name'      => $this->name,
            'year_end'  => $this->year_end,
            'folder'    => $this->folder,
        ];

        $auditions = Audition::where('project_id', $this->id)->with(['project', 'actor', 'movie', 'role'])->get();
        $toCache = [];
        $actors = new ActorCollection([]);
        foreach ($auditions as $audition) {

            if (!isset($toCache[$audition->project_role_id])) {
                $toCache[$audition->project_role_id] = [];
            }
            $toCache[$audition->project_role_id][] = $audition;

            if (!isset($actors[$audition->actor->id])) {
                $actors[$audition->actor->id] = new ActorInProjectResource($audition->actor);
            }

            $actors[$audition->actor->id]->addAudition(new AuditionResource($audition));
        }
        StaticCache::set('auditions_in_project_' . $this->id, $toCache);
        $actors = $actors->sortBy('name');

        $data['roles'] = new RoleInProjectCollection($this->roles);
        $data['actors'] = new ActorInProjectCollection($actors);

        return $data;
    }

    public static function collection($resource){
        return new ProjectCollection($resource, get_called_class());
    }
}
