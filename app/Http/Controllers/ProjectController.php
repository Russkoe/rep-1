<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProjectListResource;
use App\Http\Resources\ProjectResource;
use App\Http\Resources\RoleInProjectResource;
use App\Models\Project;
use App\Models\Role;
use Illuminate\Http\Request;
use Inertia\Inertia;


class ProjectController extends Controller
{

    /**
     * @param Request $request
     * @return \Inertia\Response
     */
    public function getProjects(Request $request)
    {
        return Inertia::render('Projects', []);
    }

    /**
     * @param Request $request
     * @return \Inertia\Response
     */
    public function project(Request $request)
    {
        $project = Project::findOrFail($request->id);
        return Inertia::render('Project', [
            'project' => (new ProjectResource($project))->withMovies(false),
        ]);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function deleteProject(Request $request)
    {
        if ($request->ajax()) {
            $status = 'fail';
            $project = Project::findOrFail($request->project['id']);

            if($project) {
                $project->movies()->forceDelete();
                $project->roles()->forceDelete();
                $project->auditions()->forceDelete();
                $project->forceDelete();
                $status = 'success';
            }

            return ['status' => $status];
        }

        abort(422);
        return response();
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function ajaxProject(Request $request)
    {
        if ($request->ajax()) {
            $data = ['status' => 'fail'];
            $project = Project::findOrFail($request->id);
            if($project) {
                $data = ['status' => 'success', 'project' => $project];
            }

            return $data;
        }

        abort(422);
        return response();
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function ajaxUpdateProject(Request $request)
    {
        if ($request->ajax()) {
            $data = ['status' => 'fail'];
            $project = Project::findOrFail($request->project['id']);

            if($project) {
                $pathUpdated = $request->project['folder'] !== $project->folder;

                $project->update([
                       'year_end' => $request->project['year'],
                       'folder'   => $request->project['folder'],
                    ]);

                if($pathUpdated) {
                    $class = '\\App\\Jobs\\ReIndexKinoFiles';
                    dispatch(new $class($request->project['folder']));
                }

                return ['status' => 'success', 'project' => $project];
            }


            return $data;
        }

        abort(422);
        return response();
    }

    /**
     * @param $projectId
     * @param $roleId
     * @return \Inertia\Response
     */
    public function projectRole($projectId, $roleId)
    {
        $roleResource = null;

        // project
        $project = Project::find($projectId);

        // role
        $role = Role::findOrFail($roleId);
        if($role) {
            $roleResource = new RoleInProjectResource($role);
        }

        return Inertia::render('ProjectRole', [
            'project'  => $project,
            'role'     => $roleResource,
        ]);
    }
}
