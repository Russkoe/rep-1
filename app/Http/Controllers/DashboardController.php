<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Actor;
use App\Http\Resources\ActorListResource;
use App\Models\Project;
use App\Http\Resources\ProjectListResource;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        return Inertia::render('Dashboard', []);
    }
}
