<?php

namespace App\Http\Controllers;

use App\Http\Resources\LogCollection;
use App\Models\Actor;
use App\Models\Audition;
use App\Models\Log;
use App\Models\Movie;
use App\Models\Project;
use App\Models\Role;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Storage;

class SettingsController extends Controller
{
    /**
     * @param Request $request
     * @return \Inertia\Response
     */
    public function index(Request $request)
    {
        $filename = 'storage.txt';
        $file  = null;
        $array = [];
        if (Storage::disk('kino')->exists($filename)) {
            $file =  Storage::disk('kino')->get($filename);

            foreach (explode("\n", $file) as $key=>$line){
                array_push($array, $line);
            }
        }

        return Inertia::render('Settings', [
            'errors' => new LogCollection(Log::where('type', 'error')->orderBy('id', 'desc')->take(50)->get()),
            'debugs' => new LogCollection(Log::where('type', 'debug')->orderBy('id', 'desc')->take(50)->get()),
            'file'   => $array,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|string
     */
    public function ajaxStorageGet(Request $request)
    {
        if ($request->ajax()) {
            $filename = 'storage.txt';

            if (!Storage::disk('kino')->exists($filename)) {
                Storage::disk('kino')->put($filename, '');
            }

            return  Storage::disk('kino')->get($filename);
        }

        abort(422);
        return response();
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|string
     */
    public function ajaxStorageUpdate(Request $request)
    {
        if ($request->ajax()) {
            $filename = 'storage.txt';

            if (!Storage::disk('kino')->exists($filename)) {
                Storage::disk('kino')->put($filename, '');
            }

            Storage::disk('kino')->put($filename, $request->get('logs'));
            return $request->get('logs');
        }

        abort(422);
        return response();
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function storageData(Request $request)
    {
        if ($request->ajax()) {
            $class = '\\App\\Jobs\\ReIndexKinoFiles';
            $id = $request->id;
            if($id) {
                $project = Project::find($id);
                dispatch(new $class($project->folder));
            } else {
                dispatch(new $class());
            }

            return [
                    'status' => 'success',
                    'errors' => new LogCollection(Log::where('type', 'error')->orderBy('id', 'desc')->take(50)->get()),
                    'debugs' => new LogCollection(Log::where('type', 'debug')->orderBy('id', 'desc')->take(50)->get())];
        }

        abort(422);
        return response();
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function clearLogs(Request $request)
    {
        if ($request->ajax()) {
            Log::truncate();
            return ['status' => 'success'];
        }

        abort(422);
        return response();
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function truncateAllData(Request $request)
    {
        if ($request->ajax()) {

            // reindex all data
            $class = '\\App\\Jobs\\ReIndexKinoFiles';
            dispatch(new $class(null, true));

            return ['status' => 'success'];
        }

        abort(422);
        return response();
    }
}
