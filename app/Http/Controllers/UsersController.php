<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * @param Request $request
     * @return \Inertia\Response
     */
    public function index(Request $request)
    {
        return Inertia::render('Users', ['users' => new UserCollection(User::get())]);
    }

    /**
     * @param Request $request
     * @return \Inertia\Response
     */
    public function user(Request $request)
    {
        return Inertia::render('User', [
            'user' => new UserResource(User::findOrFail($request->id)),
        ]);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function updateUser(Request $request)
    {
        if ($request->ajax()) {
            $status = 'fail';
            $user = User::findOrFail($request->form['id']);

            $passwordsMatch = $passwordsMatch = $request->form['password'] == $request->form['password_confirmation'];
            if($user && $passwordsMatch) {
                $data = [
                  'name'      =>  $request->form['name'],
                  'email'     =>  $request->form['email'],
                  'password'  =>  Hash::make($request->form['password']),
                ];

                $user->update($data);
                $status = 'success';
            }

            return ['status' => $status];
        }

        abort(422);
        return response();
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function deleteUser(Request $request)
    {
        if ($request->ajax()) {
            $status = 'fail';
            $user = User::findOrFail($request->form['id']);

            if($user) {
                $status = 'success';
                $user->delete();
            }

            return ['status' => $status];
        }

        abort(422);
        return response();
    }
}
