<?php

namespace App\Http\Controllers;

use App\Http\Resources\RoleListResource;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\Actor;
use App\Models\Project;
use App\Http\Resources\ProjectListResource;
use App\Http\Resources\ActorListResource;

class SearchController extends Controller
{
    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Resources\Json\AnonymousResourceCollection|\Illuminate\Http\Response
     */
    public function actors(Request $request)
    {
        if ($request->ajax()) {
            $name = $request->input('name');
            $orderBy = $request->input('order_by');
            $actors = Actor::query();

            if(!empty($name)) {
                $actors->where('name', 'like', '%' . $name . '%');
            }

            if(!empty($orderBy))
            {
                if($orderBy === 'date') {
                    $actors->orderBy('id', 'DESC');
                }

                if($orderBy === 'name') {
                    $actors->orderBy('name');
                }

                return ActorListResource::collection($actors->paginate(20));
            }

            return  ActorListResource::collection($actors->orderBy('id', 'DESC')->orderBy('name')->paginate(20));
        }

        abort(422);
        return response();
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Resources\Json\AnonymousResourceCollection|\Illuminate\Http\Response
     */
    public function projects(Request $request)
    {
        if ($request->ajax()) {
            $name = $request->input('name');
            $projects = Project::query();

            if(!empty($name)) {
                $projects->where('name', 'like', '%' . $name . '%');
            }

            return  ProjectListResource::collection(
                $projects->orderBy('sort_ord', 'ASC')->orderBy('name', 'ASC')->paginate(20));
        }

        abort(422);
        return response();
    }

    /**
     * @param Request $request
     * @return \App\Http\Resources\RoleCollection|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Resources\Json\AnonymousResourceCollection|\Illuminate\Http\Response
     */
    public function roles(Request $request)
    {
        if ($request->ajax()) {
            $name = $request->input('name');
            $roles = Role::query();

            if(!empty($name)) {
                $roles->where('name', 'like', '%' . $name . '%');
            }

            return  RoleListResource::collection($roles->orderBy('name')->paginate(20));
        }

        abort(422);
        return response();
    }
}
