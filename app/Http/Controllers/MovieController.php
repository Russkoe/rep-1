<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use App\Kino\VideoStream;

class MovieController extends Controller
{
    public function downloadMovie($id)
    {
        $movie = Movie::findOrFail($id);
        $disk = Storage::disk('kino');

        header('HTTP/1.0 200 OK');
        header('Content-Type: video/' . ($movie->extension === 'mp4' ? 'mp4' : 'quicktime' ));
        header('Accept-Ranges: bytes');
        header('Content-Length:'.$disk->getSize($movie->file_path));
        header("Content-Transfer-Encoding: binary\n");
        header("Content-Disposition: inline;");

        $stream = $disk->readStream($movie->file_path);

        while (!feof($stream)) {
            echo fread($stream, 102400);
            ob_flush();
            flush();
        }

        exit;
    }

    public function displayImage($filePath)
    {
        $movie = Movie::find($filePath);
        if (!$movie) {
            abort(404);
        }

        if(empty($movie->image_path)) {
            $path = public_path('/img/wait_screen.jpg');
        } elseif($movie->image_path === 'error.jpg') {
            $path = public_path('/img/error_screen.jpg');
        } else {
            $path = Storage::disk('movie_screenshots')->path($movie->image_path);
        }

        if (!\File::exists($path)) {
            abort(404);
        }

        $file = \File::get($path);
        $type = \File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;

    }
}


