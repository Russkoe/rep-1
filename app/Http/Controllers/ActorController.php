<?php

namespace App\Http\Controllers;

use App\Http\Resources\ActorResource;
use App\Http\Resources\ActorListResource;
use Illuminate\Http\Request;
use App\Models\Actor;
use Inertia\Inertia;

class ActorController extends Controller
{
    /**
     * @param Request $request
     * @return \Inertia\Response
     */
    public function getActors(Request $request)
    {
        return Inertia::render('Actors', []);
    }


    /**
     * @param Request $request
     * @return \Inertia\Response
     */
    public function actor(Request $request)
    {
        return Inertia::render('Actor', [
            'actor' => (new ActorResource(Actor::findOrFail($request->id)))
                ->withAuditions(true)->withMoviesGroupedByProject(true),
        ]);
    }

    public function deleteActor(Request $request)
    {
        if ($request->ajax()) {
            $status = 'fail';
            $actor = Actor::findOrFail($request->actor['id']);

            if($actor) {
                $actor->auditions()->forceDelete();
                $actor->forceDelete();
                $status = 'success';
            }

            return ['status' => $status];
        }

        abort(422);
        return response();
    }
}
