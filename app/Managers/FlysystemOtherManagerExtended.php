<?php

namespace App\Managers;

use Danhunsaker\Laravel\Flysystem\FlysystemOtherManager;
use Danhunsaker\Laravel\Flysystem\FlysystemManager;

class FlysystemOtherManagerExtended extends FlysystemManager
{
    /**
     * {@inheritdoc}
     */
    public function __construct($app)
    {
        parent::__construct($app);


        if (class_exists('\RobGridley\Flysystem\Smb\SmbAdapter')) {
            $this->extend('smb', function ($app, $config) {
                if (class_exists('\Icewind\SMB\Server')) {
                    $server = new \Icewind\SMB\Server($config['host'], $config['username'], $config['password']);
                } elseif (class_exists('\Icewind\SMB\ServerFactory')) {
                    $server = with(new \Icewind\SMB\ServerFactory)->createServer($config['host'], new \Icewind\SMB\BasicAuth($config['username'], $config['workgroup'], $config['password']));
                }
                $share = $server->getShare($config['path']);

                return $this->createFlysystem(new \RobGridley\Flysystem\Smb\SmbAdapter($share), $config);
            });
        }


    }

    protected function buildMirrors($disks)
    {
        $main = $this->disk(Arr::first($disks))->getAdapter();

        if (count($disks) > 2) {
            $second = $this->buildMirrors(array_slice($disks, 1));
        } else {
            $second = $this->disk(Arr::last($disks))->getAdapter();
        }

        return new \League\Flysystem\Replicate\ReplicateAdapter(new \Litipk\Flysystem\Fallback\FallbackAdapter($main, $second, true), $second);
    }

    /**
     * {@inheritdoc}
     */
    protected function resolve($name, $config = NULL)
    {
        $adapter = parent::resolve($name, $config);

        if (class_exists('Twistor\FlysystemStreamWrapper')) {
            \Twistor\FlysystemStreamWrapper::register($name, $adapter->getDriver());
        }

        return $adapter;
    }
}