<?php
/**
 * Created by PhpStorm.
 * User: nikolay
 * Date: 03.06.21
 * Time: 22:10
 */

namespace App\Kino;


use App\Models\Actor;
use App\Models\Audition;
use App\Models\Movie;
use App\Models\Project;
use App\Models\Role;
use Illuminate\Support\Facades\Storage;

class PathParser
{
    private $path;

    const MP4_FORMAT = 'mp4';
    const MOV_FORMAT = 'mov';

    /**
     * PathParser constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        $isHidden = substr($this->getMovieName(), 0, 1) === '.';
        if($isHidden) {
            return false;
        }

        $extension = strtolower(pathinfo($this->path, PATHINFO_EXTENSION));
        return $extension === self::MOV_FORMAT || $extension === self::MP4_FORMAT;
    }

    public function getProjectName(): string
    {
        $pathinfo = pathinfo($this->path, PATHINFO_DIRNAME);
        $pathinfo = array_filter( explode('/', $pathinfo) );
        $result = array_pop($pathinfo);
        return $result;
    }

    public function getFolder(): string
    {
        return  pathinfo($this->path, PATHINFO_DIRNAME);
    }

    public function getProject(): Project
    {
        $project = Project::where('folder', $this->getFolder())
            ->orWhere('name', $this->getProjectName())->first();

        $projectName = $this->getProjectName();
        $projectFolder = $this->getFolder();

        $yearEnd = $this->getYearEnd();
        $sortOrd = null;

        if(!$yearEnd) {
            $sortOrd = 0;
        } else {
            $sortOrd = intval(date('Y')) - $yearEnd;
        }

        if(!$project) {
            $project           = new Project();
            $project->name     = $projectName;
            $project->folder   = $projectFolder;
            $project->year_end = $yearEnd;
            $project->sort_ord = $sortOrd;
            $project->save();
        } else {
            $data = [
                'name'   => $projectName,
                'folder' => $projectFolder,
            ];

            if(!$project->year_end) {
                if($yearEnd) {
                    $data['year_end'] = $yearEnd;
                }
            }

            $project->sort_ord = $sortOrd;
            $project->update($data);
        }

        return $project;
    }

    public function getMovieName(): string
    {
        $explode = explode('/', $this->path);
        return  end($explode);
    }

    public function getMovie(): Movie
    {
        $movieName = $this->getMovieName();
        $project = Project::where('name', $this->getProjectName())->first();

        $movie = Movie::where('project_id', $project->id)
            ->where(function ($query) use ($movieName) {
                $query->where('file_path', $this->path)
                    ->orWhere('name', $movieName);
            })->first();

        $extention = $this->getExt();

        if(!$movie) {
            $movie             = new Movie();
            $movie->name       = $movieName;
            $movie->project_id = $project->id;
            $movie->file_path  = $this->path;
            $movie->extension  = $extention;
            $movie->size       = $this->getSize();
            $movie->save();
        } else {
            $movie->update([
                'file_path'  => $this->path,
                'extension'  => $extention,
            ]);
        }

        return $movie;

    }

    public function getExt(): string
    {
        return strtolower(pathinfo($this->path, PATHINFO_EXTENSION));
    }

    public function getSize(): int
    {
       return Storage::disk('kino')->size($this->path);
    }

    public function parseRoles(): array
    {
        //арина - витрук юлия_алекс - эюб фараджев_дима - журавлев сергей.mp4
        //алекс - розин дан.mp4
        //адельфина виктория.mp4
        //белугин даниил_дубинин владимир_юхневич алина_1.mp4
        //богомазова ольга__арзуманов семен__немцев федор_1
        //гребе юлиана_иванов виталий_тяптушкин владимир_2
        //мать невесты - фелялина мария.mp4
        //михеева евгения__щербина юрий_сергеев арсений_1

        $movie = $this->getMovie();
        $name = mb_substr($movie->name, 0, mb_strrpos($movie->name, "."));
        //убираем все цифры из файла
        $name = preg_replace("/^[0-9]{1,2}(_|-)/uis","",$name);
        $name = preg_replace("/[0-9 _-]{1,}$/uis","",$name);
        $name = trim($name, "-_ ");

        $name = preg_replace("/( ){2,}/u", " ", $name);
        $actorsRoles = explode("_", $name);
        foreach ($actorsRoles as $i => $actorsRole) {
            if ($actorsRole === '') {
                unset($actorsRoles[$i]);
            }
        }



        $actors = [];
        foreach ($actorsRoles as $actorsRole) {
            $data = explode("-", $actorsRole);
            if (count($data) === 1) {
                $actors[trim($data[0])] = null;
            } else {
                $actors[trim($data[1])] = trim($data[0]);
            }
        }

        return $actors;
    }

    public function getAuditions(): array
    {
        $actors = $this->parseRoles();

        $project = $this->getProject();
        $movie = $this->getMovie();
        $auditions = [];

        foreach ($actors as $actorName => $roleName) {

            $actor = Actor::where('name', $actorName)->first();
            if (!$actor) {
                $actor = new Actor();
                $actor->name = $actorName;
                $actor->save();
            }

            $role = null;

            if (!empty($roleName)) {
                $role = Role::where('project_id', $project->id)->where('name', $roleName)->first();
                if (!$role) {
                    $role = new Role();
                    $role->name = $roleName;
                    $role->project_id = $project->id;
                    $role->save();
                }
            }

            $audition = Audition::where('project_id', $project->id)
                ->where('project_movie_id', $movie->id)
                ->where('actor_id', $actor->id)
                ->first();

            if (!$audition) {
                $audition = new Audition();
                $audition->project_id = $project->id;
                $audition->project_movie_id = $movie->id;
            }

            $audition->actor_id = $actor->id;
            if ($role) {
                $audition->project_role_id = $role->id;
            }
            $audition->save();

          //  dump($audition->getAttributes());
           // dump($actor->getAttributes());
            if ($role) {
             //   dump($role->getAttributes());
            } else {
               // dump($role);
            }


            $auditions[] = $audition;
        }

        return $auditions;
    }

    public function getYearEnd()
    {
        if (preg_match_all('/\b\d{4}\b/', $this->path, $matches)) {
            return end($matches[0]);
        }
    }
}