<?php

namespace App\Kino;

class Helper
{
    /**
     * ucfirst для многобайтных строк
     * @param  string $string строка для изменения
     * @return string|null
     */
    public static function ucfirst(?string $string):? string
    {
        return $string ? mb_strtoupper(mb_substr($string, 0, 1)).mb_substr($string, 1) : null;
    }

    /**
     * @param $endings
     * @param $number
     * @return string
     */
    public static function plural($endings, $number)
    {
        $cases = [2, 0, 1, 1, 1, 2];
        $n = $number;
        return sprintf($endings[ ($n%100>4 && $n%100<20) ? 2 : $cases[min($n%10, 5)] ], $n);
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function startsWith($haystack, $needle) {
        return substr_compare($haystack, $needle, 0, strlen($needle)) === 0;
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function endsWith($haystack, $needle) {
        return substr_compare($haystack, $needle, -strlen($needle)) === 0;
    }
}