<?php

namespace App\Providers;

use Danhunsaker\Laravel\Flysystem\FlysystemOtherServiceProvider;
use App\Managers\FlysystemOtherManagerExtended;

class FlysystemOtherServiceProviderExtended extends FlysystemOtherServiceProvider
{

    /**
     * Register the filesystem manager.
     *
     * @return void
     */
    protected function registerManager()
    {
        $this->app->singleton('filesystem', function () {
            return new FlysystemOtherManagerExtended($this->app);
        });
    }
}