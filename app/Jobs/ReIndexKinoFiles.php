<?php

namespace App\Jobs;

use App\Models\Actor;
use App\Models\Audition;
use App\Models\Movie;
use App\Models\Project;
use App\Models\Role;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use App\Models\Log;
use App\Kino\PathParser;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;


class ReIndexKinoFiles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var boolean
     */
    protected $truncate;

    public $timeout = 0;

    /**
     * Create a new command instance.
     *
     * @param  $path
     * @param $truncate
     */
    public function __construct($path = null, $truncate = false)
    {
       $this->path = $path;
       $this->truncate = $truncate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->addDebug('Старт переиндексации path='. $this->path .' truncate='.(int)$this->truncate,'');
        if($this->truncate) {

            // truncate tables
            Project::truncate();
            Actor::truncate();
            Log::truncate();
            Role::truncate();
            Movie::truncate();
            Audition::truncate();

            // clear screenshots
            $files =   Storage::disk('movie_screenshots')->allFiles();

            foreach ($files as $file) {
                Storage::disk('movie_screenshots')->delete($file);
            }
        }

        if($this->path) {
           $this->indexKinoFiles($this->path);
           return;
        }

        $filename = 'storage.txt';
        if (Storage::disk('kino')->exists($filename)) {
            $file = Storage::disk('kino')->get($filename);
            foreach (explode("\n", $file) as $key => $line) {
                try {
                    $this->indexKinoFiles($line);
                } catch (\Exception $e) {
                    $this->addError('Некорректное имя видео', $line);
                }
            }
        }
    }

    /**
     * @param $line
     */
    protected function indexKinoFiles($line)
    {
        $this->addDebug('Индексация директории',$line);

        $disk = Storage::disk('kino');
        if(!$disk->exists($line)) {
            return;
        }

        $line = rtrim($line, '/');
        $projects = $disk->allDirectories($line);
        if (!count($projects) && $this->path !== null) {
            $projectDirectory = Project::where('folder', $this->path)->first();
            if ($projectDirectory) {
                $projects[] = $this->path;
            }
        }
        foreach ($projects as $project) {
            $files = $disk->files($project);
            foreach ($files as $file) {
                if(str_ends_with($file, '.DS_Store')) {
                    continue;
                }

                $parser = new PathParser($file);
                if ($parser->isValid()) {
                    $parser->getProject();
                    $parser->getMovie();
                    $parser->getAuditions();
                    $this->addDebug('Успешно обработано видео', $file);
                } else {
                    $this->addError('Некорректное имя видео', $file);
                }
            }
        }
    }


    /**
     * @param $text
     * @param string $debug
     */
    protected function addError($text, $debug=null) {
        $data = [
            'text'  => $text,
            'debug' => $debug,
            'type'  => 'error',
        ];

        Log::create($data);
    }

    /**
     * @param $text
     * @param string $debug
     */
    protected function addDebug($text, $debug=null) {
        $data = [
            'text'  => $text,
            'debug' => $debug,
            'type'  => 'debug',
        ];

        Log::create($data);
    }

}
