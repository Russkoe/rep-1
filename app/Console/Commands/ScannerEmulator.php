<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ScannerEmulator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'projects:scan:emulate {disk_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Сканирует все директории и файлы на диске disk_name и в папке 
           storage/app/emulator/{disk_name} создает точно такое же дерево директорий,
            в директориях создает пустые файлы с тем же именем и раширением,
            в каждый файл записывает json-текст {"size": реальный размер исходного файла}';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $diskName   = $this->argument('disk_name');
        $srcFolder = $diskName ?? storage_path('app/folder');

        $this->recurseCopy( $srcFolder,
            storage_path('app/emulator'));
        return 0;
    }

    protected function recurseCopy($src, $dst) {

        $dir = opendir($src);

        if (!file_exists($dst)) {
            mkdir($dst, 0777, true);
        }

        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    $this->recurseCopy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file, $dst . '/' . $file.'.txt');

                    $this->line('File Name ----> '.$src . '/' . $file);
                    $fileSize = filesize($src . '/' . $file);

                    $this->line('File Size ----> '.$fileSize);

                    $this->line('----------------------------------------'.PHP_EOL);

                    $json = json_encode(array('size' => $fileSize));
                    file_put_contents($dst . '/' . $file.'.txt', $json);
                }
            }
        }

        closedir($dir);
    }
}
