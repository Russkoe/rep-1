<?php

namespace App\Console\Commands;

use App\Models\Movie;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Lakshmaji\Thumbnail\Thumbnail;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;

class  CreateScreenShots extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'screen-shots:handle {--num=4}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создает скриншоты для project_movies';


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // выбирает 2 movies, у которых нет скриншота
        $movies = Movie::where('image_path', null)->orderBy('id', 'DESC')->take($this->option('num'))->get();

        // скриншоты складываем локально в папку /storage/movies/{id}.jpg, если уже есть, то перезаписываем
        $thumbnailPath   = Storage::disk('movie_screenshots')->getDriver()->getAdapter()->getPathPrefix();
        $videoPath       = Storage::disk('temp_videos')->getDriver()->getAdapter()->getPathPrefix();

        foreach ($movies as $movie) {
          dump($movie->id .' ' . $movie->name);
            $movie->update([
                'image_path' => 'error.jpg',
            ]);

          try {
              // копируем локально на сервер в папку storage/tmp видео
              Storage::disk('temp_videos')->put($movie->name, Storage::disk('kino')->get($movie->file_path));

              $screenShotId = $movie->id;
              $thumbnailImage  = $screenShotId.".jpg";

              //  создает скриншот на 20 секунде видео через ffmpeg
              $timeToImage    = 20;


              $th = new Thumbnail();
              $thumbnail_status = $th->getThumbnail(
                  $videoPath . $movie->name,
                  $thumbnailPath,
                  $thumbnailImage,
                  $timeToImage
              );

              if($thumbnail_status) {
                  dump("Thumbnail generated");

                  // set image_path for movie
                  $movie->update([
                      'image_path' => $movie->id.'.jpg',
                  ]);
              } else {
                  $this->error("Thumbnail generation has failed for ".$movie->id.".jpg");
              }

          } catch (\Exception $exception) {
              $this->error($exception->getMessage());
          }

        }

        // все файлы в temp_videos
        $files =   Storage::disk('temp_videos')->allFiles();

        // удаление файлов
        Storage::disk('temp_videos')->delete($files);

        // все директории в temp_videos
        $dirs = Storage::disk('temp_videos')->directories();

        foreach ($dirs as $dir) {
            Storage::disk('temp_videos')->deleteDirectory($dir);
        }

        return 0;
    }
}
