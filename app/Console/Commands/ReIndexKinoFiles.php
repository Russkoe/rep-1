<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ReIndexKinoFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 're-index:handle {--path=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Переиндексатор';


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path         = $this->option('path');

        $class = '\\App\\Jobs\\ReIndexKinoFiles';
        dispatch(new $class($path))->onConnection('sync');

        return 0;
    }
}
