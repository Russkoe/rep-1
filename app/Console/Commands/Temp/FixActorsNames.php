<?php

namespace App\Console\Commands\Temp;

use App\Kino\Helper;
use Illuminate\Console\Command;
use App\Models\Actor;

class FixActorsNames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'actors:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove extra spaces from actors names';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Actor::query()
            ->chunk(200, function ($actors): void {
                foreach ($actors as $actor) {
                   if(Helper::startsWith($actor->name, " ")) {
                       dump($actor->name);

                       $actor->update([
                           'name' => trim($actor->name),
                       ]);
                   }
                }
            });

        return 0;
    }
}
