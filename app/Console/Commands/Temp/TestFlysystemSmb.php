<?php

namespace App\Console\Commands\Temp;

use Illuminate\Console\Command;
use Icewind\SMB\BasicAuth;
use Icewind\SMB\ServerFactory;
use League\Flysystem\Filesystem;
use RobGridley\Flysystem\Smb\SmbAdapter;
use Illuminate\Support\Facades\Storage;

class TestFlysystemSmb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smb:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверка чтения из Smb';


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $smb = Storage::disk('kino');
        dump($smb);
        $stream = $smb->readStream("Casting Catalogs/Съемки окончены/2015/Исцеление/оля - фаттахова олеся _ денис - кислов петр.mov");

        while (!feof($stream)) {
            echo fread($stream, 1024);
            flush();
        }

        return 0;

    }
}
