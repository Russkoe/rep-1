<?php

declare(strict_types=1);

namespace App\Services\Cache;

class StaticCache
{
    /** @var array */
    private static $_cache;

    /**
     * @param int|null $key
     *
     * @return mixed
     */
    public static function get($key)
    {
        if (isset(self::$_cache[$key])) {
            return self::$_cache[$key];
        }

        return null;
    }

    /**
     * @param $key
     * @param $value
     */
    public static function set($key, $value)
    {
        self::$_cache[$key] = $value;
    }
}