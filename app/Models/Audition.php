<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Audition
 *
 * @property int $id
 * @property int $actor_id
 * @property int $project_movie_id
 * @property int|null $project_role_id
 * @property int|null $project_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Audition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Audition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Audition query()
 * @method static \Illuminate\Database\Eloquent\Builder|Audition whereActorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Audition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Audition whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Audition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Audition whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Audition whereProjectMovieId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Audition whereProjectRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Audition whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Audition extends Model
{
    use SoftDeletes;

    /**
     * @return BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function movie()
    {
        return $this->belongsTo(Movie::class, 'project_movie_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function actor()
    {
        return $this->belongsTo(Actor::class, 'actor_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'project_role_id', 'id');
    }



}
