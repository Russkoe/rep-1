<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Movie
 *
 * @property int $id
 * @property int|null $project_id
 * @property string $name
 * @property string|null $file_path
 * @property string|null $image_path
 * @property string|null $extension
 * @property int|null $size
 * @property int|null $error_code
 * @property mixed|null $params
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Movie newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Movie newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Movie query()
 * @method static \Illuminate\Database\Eloquent\Builder|Movie whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Movie whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Movie whereErrorCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Movie whereExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Movie whereFilePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Movie whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Movie whereImagePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Movie whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Movie whereParams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Movie whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Movie whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Movie whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Movie extends Model
{
    use SoftDeletes;

    protected $table = 'project_movies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id',
        'name',
        'file_path',
        'image_path',
        'extension',
        'size',
        'error_code',
        'params',
        'created_at',
        'updated_at',
        'deleted_at',
    ];


    /**
     * @return BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    public function auditions()
    {
        return $this->hasMany(Audition::class, 'project_movie_id', 'id');
    }

    public function scopeByActor(Builder $query, int $actorId)
    {
        return $query->join('auditions', 'auditions.project_movie_id', '=', 'project_movies.id')
            ->where('auditions.actor_id', $actorId);
    }
}
