<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Project
 *
 * @property int $id
 * @property string $name
 * @property string|null $year_end
 * @property string|null $folder
 * @property int $is_finished
 * @property mixed|null $params
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $sort_ord
 * @method static \Illuminate\Database\Eloquent\Builder|Project newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Project newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Project query()
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereFolder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereIsFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereParams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereYearEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereSortOrd($value)
 * @mixin \Eloquent
 */
class Project extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'year_end',
        'folder',
        'is_finished',
        'params',
        'created_at',
        'updated_at',
        'deleted_at',
        'sort_ord',
    ];



    /**
     *
     * @return HasMany
     */
    public function roles()
    {
        return $this->hasMany(Role::class, 'project_id', 'id');
    }

    /**
     *
     * @return HasMany
     */
    public function movies()
    {
        return $this->hasMany(Movie::class, 'project_id', 'id');
    }

    /**
     * @return BelongsToMany
     */
    public function actors () {
        return $this->belongsToMany(Actor::class,  'auditions', 'project_id', 'actor_id')->groupBy('actor_id');
    }

    /**
     * @return HasMany
     */
    public function auditions()
    {
        return $this->hasMany(Audition::class, 'project_id', 'id');
    }
}
