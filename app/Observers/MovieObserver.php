<?php

namespace App\Observers;

use App\Models\Movie;
use Illuminate\Support\Facades\Storage;

class MovieObserver
{
    /**
     * Handle the Movie "deleting" event.
     *
     * @param Movie $movie
     * @return void
     */
    public function deleting(Movie $movie)
    {
        Storage::disk('movie_screenshots')->delete($movie->id.'.jpg');
    }
}
