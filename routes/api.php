<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\ProjectController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([
    ['prefix' => 'api'],
], function (): void {

    // actors
    Route::get('/actors', [SearchController::class, 'actors'])->name('loadActors');

    // projects
    Route::get('/projects', [SearchController::class, 'projects'])->name('loadProjects');

    // roles
    Route::get('/roles', [SearchController::class, 'roles'])->name('loadRoles');

    // logs
    Route::get('/storage/get', [SettingsController::class, 'ajaxStorageGet']);

    Route::post('/storage/update', [SettingsController::class, 'ajaxStorageUpdate']);

    Route::get('/storage/indexData/{id}', [SettingsController::class, 'storageData']);
    Route::get('/storage/indexData', [SettingsController::class, 'storageData']);

    // edit project
    Route::get('/project/get/{id}', [ProjectController::class, 'ajaxProject']);

    Route::post('/project/update/{id}', [ProjectController::class, 'ajaxUpdateProject']);

});