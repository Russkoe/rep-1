<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ActorController;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Controller::class, 'login']);

Route::group([
    'middleware' => [
        'auth:sanctum',
        'verified',
    ],
], function (): void {
    // pages
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    // actors
    Route::get('/actors', [ActorController::class, 'getActors'])->name('actors');
    Route::get('/actor/{id}', [ActorController::class, 'actor'])->name('actor');
    Route::post('/deleteActor', [ActorController::class, 'deleteActor'])->name('deleteActor');

    // projects
    Route::get('/projects', [ProjectController::class, 'getProjects'])->name('projects');
    Route::get('/project/{id}', [ProjectController::class, 'project'])->name('project');
    Route::post('/deleteProject', [ProjectController::class, 'deleteProject'])->name('deleteProject');
    Route::get('/project/{projectId}/role/{roleId}', [ProjectController::class, 'projectRole'])->name('projectRole');

    // search
    Route::get('/ajaxSearch', [SearchController::class, 'search'])->name('search');

    // settings
    Route::get('/settings', [SettingsController::class, 'index'] )->name('settings');
    Route::get('/clearLogs', [SettingsController::class, 'clearLogs'])->name('clearLogs');
    Route::post('/truncateAllData', [SettingsController::class, 'truncateAllData'])->name('truncateAllData');

    // users
    Route::get('/users', [UsersController::class, 'index'])->name('users');
    Route::get('/user/{id}', [UsersController::class, 'user'])->name('user');
    Route::post('/updateUser', [UsersController::class, 'updateUser'])->name('updateUser');
    Route::post('/deleteUser', [UsersController::class, 'deleteUser'])->name('deleteUser');


    // movies
    Route::get('/movie/{id}', [MovieController::class, 'downloadMovie'])->name('movie');
    Route::get('/screenshotImage/{id}', [MovieController::class, 'displayImage'])->name('screenshotImage.displayImage');
});
