<?php

namespace Deployer;

require __DIR__ . '/vendor/autoload.php';

// All Deployer recipes are based on `recipe/common.php`.
require 'recipe/laravel.php';

inventory('.deploy/servers.yml');

$stage = '';
set('shared_dirs', [
    'storage',
]);

set('shared_files', [
    '.env',
]);

set('repository', 'git@bitbucket.org:opwb/kino.git');
set('keep_releases', 3);
set('writable_use_sudo', false);
set('writable_dirs', [
    'bootstrap/cache',
    'storage',
    'storage/framework/sessions',
    'storage/framework/cache',
    'storage/framework/views',

]);

/**
 * Create symlinks for shared directories and files.
 */
task('deploy:shared', function () {
    $sharedPath = "{{deploy_path}}/shared";

    foreach (get('shared_dirs') as $dir) {
        // Remove from source.
        run("if [ -d $(echo {{release_path}}/$dir) ]; then rm -rf {{release_path}}/$dir; fi");

        // Create shared dir if it does not exist.
        run("mkdir -p $sharedPath/$dir");

        // Create path to shared dir in release dir if it does not exist.
        // (symlink will not create the path and will fail otherwise)
        run("mkdir -p `dirname {{release_path}}/$dir`");

        // Symlink shared dir to release dir
        run("ln -nfs $sharedPath/$dir {{release_path}}/$dir");
    }

    foreach (get('shared_files') as $file) {
        if (is_array($file)) {
            $sourceFile = array_keys($file);
            $sourceFile = reset($sourceFile);
            $destinationFile = reset($file);
        } else {
            $sourceFile = $file;
            $destinationFile = $file;
        }

        $dirname = dirname($destinationFile);
        // Remove from source.
        run("if [ -f $(echo {{release_path}}/$destinationFile) ]; then rm -rf {{release_path}}/$destinationFile; fi");
        // Ensure dir is available in release
        run("if [ ! -d $(echo {{release_path}}/$dirname) ]; then mkdir -p {{release_path}}/$dirname;fi");

        // Create dir of shared file
        run("mkdir -p $sharedPath/" . dirname($sourceFile));

        // Touch shared
        run("touch $sharedPath/$sourceFile");

        // Symlink shared dir to release dir
        run("ln -nfs $sharedPath/$sourceFile {{release_path}}/$destinationFile");
    }
})->desc('Creating symlinks for shared files');

task('reload:supervisor', function () {
    run('sudo supervisorctl reread');
    run('sudo supervisorctl update');
    run('sudo supervisorctl restart all');
})->desc('Restarting supervisor jobs.');

task('upload:env', function() {
    if (input()->hasArgument('stage')) {
        $stage = input()->getArgument('stage');
    }

    upload('.deploy/.env.' . $stage, '{{deploy_path}}/shared/.env');
});

task('deploy:vendors', function () {
    run('cd {{release_path}} && composer install --no-dev');
})->desc('Running composer update.');

task('deploy:assets', function () {
    run('ln -nfs {{deploy_path}}/shared/node_modules {{release_path}}/node_modules');
    run('cd {{release_path}} && npm install --no-bin-links --silent');
    run('ln -s {{ release_path }}/node_modules {{release_path}}/public/resources');
})->desc('Running assets.');

task('artisan:opcache:clear', function () {
    $output = run('{{bin/php}} {{deploy_path}}/current/artisan opcache:clear');
    writeln('<info>' . $output . '</info>');
})->desc('Execute artisan opcache:clear');

task('artisan:view:clear', function () {
    $output = run('{{bin/php}} {{deploy_path}}/current/artisan view:clear');
    writeln('<info>' . $output . '</info>');
})->desc('Execute artisan view:clear');

task('test', function () {
    $output = runLocally('./vendor/phpunit/phpunit/phpunit');
    writeln('<info>' . $output . '</info>');
})->desc('Running tests.');

//before('deploy', 'test');

after('deploy:update_code', 'upload:env');

//after('deploy', 'deploy:assets');
after('deploy', 'artisan:migrate');
//after('deploy', 'artisan:opcache:clear');
//after('deploy', 'artisan:view:clear');
after('deploy', 'reload:supervisor');
after('rollback', 'reload:supervisor');
