<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBasicTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actors', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->json('params')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->year('year_end')->nullable();
            $table->string('folder', 1024)->nullable();
            $table->boolean('is_finished')->default(true);
            $table->json('params')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('project_movies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id')->nullable();
            $table->string('name');
            $table->string('file_path', 1024)->nullable();
            $table->string('image_path', 1024)->nullable();
            $table->string('extension', 10)->nullable();
            $table->unsignedBigInteger('size')->nullable();
            $table->unsignedInteger('error_code')->nullable();
            $table->json('params')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('project_id');
        });
        Schema::create('project_roles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id')->nullable();
            $table->string('name');
            $table->json('params')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('project_id');
        });

        Schema::create('auditions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('actor_id');
            $table->unsignedBigInteger('project_movie_id');
            $table->unsignedBigInteger('project_role_id')->nullable();
            $table->unsignedBigInteger('project_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('actor_id');
            $table->index('project_movie_id');
            $table->index('project_role_id');
            $table->index('project_id');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actors');
        Schema::dropIfExists('projects');
        Schema::dropIfExists('project_movies');
        Schema::dropIfExists('project_roles');
        Schema::dropIfExists('auditions');
    }
}
