<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Project::updateOrCreate(['id' => 1], [
            'id' => 1,
            'name' => 'Игры престолов',
            'year_end' => 2018,
            'folder' => '/disk1/2015/игры престолов',
            'is_finished' => true,
        ]);

        Project::updateOrCreate(['id' => 2], [
            'id' => 2,
            'name' => 'Викинги',
            'year_end' => 2019,
            'folder' => '',
            'is_finished' => true,
        ]);

        Project::updateOrCreate(['id' => 3], [
            'id' => 3 ,
            'name' => 'Волчий зал',
            'year_end' => 2017,
            'folder' => '',
            'is_finished' => true,
        ]);

        Project::updateOrCreate(['id' => 4], [
            'id' => 4 ,
            'name' => 'Викинг',
            'year_end' => 2016,
            'folder' => '',
            'is_finished' => true,
        ]);

        Actor::updateOrCreate(['id' => 1], [
            'id' => 1,
            'name' => 'Иванов Петр',
        ]);

        Actor::updateOrCreate(['id' => 2], [
            'id' => 2,
            'name' => 'Маргарита Овсянникова',
        ]);

        Actor::updateOrCreate(['id' => 3], [
            'id' => 3,
            'name' => 'Данила Козловский',
        ]);

        Actor::updateOrCreate(['id' => 4], [
            'id' => 4,
            'name' => 'Кэтрин Винник',
        ]);

        Actor::updateOrCreate(['id' => 5], [
            'id' => 5,
            'name' => 'Трэвис Фиммел',
        ]);

        Actor::updateOrCreate(['id' => 6], [
            'id' => 6,
            'name' => 'Алекс Хёг Андерсен',
        ]);

        Actor::updateOrCreate(['id' => 7], [
            'id' => 7,
            'name' => 'Густаф Скарсгард',
        ]);

        Actor::updateOrCreate(['id' => 8], [
            'id' => 8,
            'name' => 'Марк Райлэнс',
        ]);

        Role::updateOrCreate(['id' => 1], [
            'id' => 1,
            'project_id' => 1,
            'name' => 'Джон Сноу',
        ]);

        Role::updateOrCreate(['id' => 2], [
            'id' => 2,
            'project_id' => 1,
            'name' => 'Эмилия Кларк',
        ]);

        Role::updateOrCreate(['id' => 3], [
            'id' => 3,
            'project_id' => 2,
            'name' => 'Лагерта',
        ]);

        Role::updateOrCreate(['id' => 4], [
            'id' => 4,
            'project_id' => 3,
            'name' => 'Томас Кранмер',
        ]);

        Role::updateOrCreate(['id' => 5], [
            'id' => 5,
            'project_id' => 2,
            'name' => 'Ивар Бескостный',
        ]);

        Role::updateOrCreate(['id' => 6], [
            'id' => 6,
            'project_id' => 2,
            'name' => 'Вещий Олег',
        ]);

        Role::updateOrCreate(['id' => 7], [
            'id' => 7,
            'project_id' => 2,
            'name' => 'Флоки',
        ]);

        Role::updateOrCreate(['id' => 8], [
            'id' => 8,
            'project_id' => 2,
            'name' => 'Рагнар',
        ]);

        Role::updateOrCreate(['id' => 9], [
            'id' => 9,
            'project_id' => 4,
            'name' => 'Князь Владимир',
        ]);

        Movie::updateOrCreate(['id' => 1], [
            'id' => 1,
            'name' => '',
            'project_id' => 1,
            'file_path' => '/var/www/html/public/kino-files/В производстве/Склиф-9/лунцова елена.mp4',
            'extension' => 'mp4',
            'size' => 123000000,
            'image_path' => 'img/1.jpg',
        ]);

        Movie::updateOrCreate(['id' => 2], [
            'id' => 2,
            'name' => '',
            'project_id' => 1,
            'file_path' => '/var/www/html/public/kino-files/В производстве/Склиф-9/лунцова елена.mp4',
            'extension' => 'mp4',
            'size' => 123000000,
            'image_path' => 'img/1.jpg',
        ]);

        Movie::updateOrCreate(['id' => 3], [
            'id' => 3,
            'name' => '',
            'project_id' => 1,
            'file_path' => '/var/www/html/public/kino-files/В производстве/Склиф-9/лунцова елена.mp4',
            'extension' => 'mp4',
            'size' => 123000000,
            'image_path' => 'img/1.jpg',
        ]);

        Movie::updateOrCreate(['id' => 4], [
            'id' => 4,
            'name' => '',
            'project_id' => 3,
            'file_path' => '/var/www/html/public/kino-files/В производстве/Склиф-9/лунцова елена.mp4',
            'extension' => 'mp4',
            'size' => 0,
            'image_path' => 'img/1.jpg',
        ]);

        Movie::updateOrCreate(['id' => 5], [
            'id' => 5,
            'name' => '',
            'project_id' => 1,
            'file_path' => '/var/www/html/public/kino-files/В производстве/Склиф-9/лунцова елена.mp4',
            'extension' => 'mp4',
            'size' => 123000000,
            'image_path' => 'img/1.jpg',
        ]);

        Audition::updateOrCreate(['id' => 1], [
            'id' => 1,
            'actor_id' => 1,
            'project_movie_id' => 1,
            'project_role_id' => 1,
            'project_id' => 1,
        ]);

        Audition::updateOrCreate(['id' => 2], [
            'id' => 2,
            'actor_id' => 2,
            'project_movie_id' => 2,
            'project_role_id' => 2,
            'project_id' => 1,
        ]);

        Audition::updateOrCreate(['id' => 3], [
            'id' => 3,
            'actor_id' => 1,
            'project_movie_id' => 3,
            'project_role_id' => 1,
            'project_id' => 1,
        ]);

        Audition::updateOrCreate(['id' => 4], [
            'id' => 4,
            'actor_id' => 2,
            'project_movie_id' => 3,
            'project_role_id' => 2,
            'project_id' => 1,
        ]);

        Audition::updateOrCreate(['id' => 5], [
            'id' => 5,
            'actor_id' => 4,
            'project_movie_id' => 3,
            'project_role_id' => 3,
            'project_id' => 2,
        ]);

        Audition::updateOrCreate(['id' => 6], [
            'id' => 6,
            'actor_id' => 6,
            'project_movie_id' => 3,
            'project_role_id' => 5,
            'project_id' => 2,
        ]);

        Audition::updateOrCreate(['id' => 7], [
            'id' => 7,
            'actor_id' => 8,
            'project_movie_id' => 4,
            'project_role_id' => 4,
            'project_id' => 3,
        ]);

        Audition::updateOrCreate(['id' => 8], [
            'id' => 8,
            'actor_id' => 3,
            'project_movie_id' => 3,
            'project_role_id' => 6,
            'project_id' => 2,
        ]);

        Audition::updateOrCreate(['id' => 9], [
            'id' => 9,
            'actor_id' => 7,
            'project_movie_id' => 3,
            'project_role_id' => 7,
            'project_id' => 2,
        ]);

        Audition::updateOrCreate(['id' => 10], [
            'id' => 10,
            'actor_id' => 5,
            'project_movie_id' => 3,
            'project_role_id' => 8,
            'project_id' => 2,
        ]);

        Audition::updateOrCreate(['id' => 11], [
            'id' => 11,
            'actor_id' => 3,
            'project_movie_id' => 5,
            'project_role_id' => 9,
            'project_id' => 4,
        ]);

    }
}
